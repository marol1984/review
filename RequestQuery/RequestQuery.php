<?php declare(strict_types=1);

namespace App\Http\RequestQuery;

use App\Http\RequestQuery\Handlers\FieldsetHandler;
use App\Http\RequestQuery\Handlers\FilteringHandler;
use App\Http\RequestQuery\Handlers\SortingHandler;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

/**
 * Translates query from http request to eloquent builder.
 *
 * usage: /v1/trailers
 * ?sort=-created_at
 * &filter[street][neq]=Brabantstraat
 * &fields=license_plate,company_number,street,created_at
 *
 * Class RequestQuery
 */
final class RequestQuery
{
    /**
     * @var FilteringHandler
     */
    private $filteringHandler;
    /**
     * @var SortingHandler
     */
    private $sortingHandler;
    /**
     * @var FieldsetHandler
     */
    private $fieldsetHandler;

    /**
     * Initialize a new filter instance.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function __construct(
        FilteringHandler $filteringHandler,
        SortingHandler $sortingHandler,
        FieldsetHandler $fieldsetHandler
    ) {
        $this->filteringHandler = $filteringHandler;
        $this->sortingHandler = $sortingHandler;
        $this->fieldsetHandler = $fieldsetHandler;
    }

    /**
     * Apply the filters on the builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $builder
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function applyTo(Builder $builder): Builder
    {
        $builder = $this->filteringHandler->applyTo($builder);
        $builder = $this->fieldsetHandler->applyTo($builder);

        return $this->sortingHandler->applyTo($builder);
    }
}
