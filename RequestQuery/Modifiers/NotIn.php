<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Modifiers;

use Illuminate\Database\Eloquent\Builder;

final class NotIn implements Modifier
{
    public function modify(Builder $builder, string $field, $value = null): Builder
    {
        return $builder->whereNotIn($field, explode(',', $value));
    }
}
