<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Modifiers;

use App\Http\RequestQuery\Modifiers\EqualTo;
use App\Http\RequestQuery\Modifiers\Modifier;
use App\Http\RequestQuery\Modifiers\NotEqualTo;
use App\Http\RequestQuery\Modifiers\OrderBy;
use App\Http\RequestQuery\Modifiers\SelectOnly;

final class ModifierFactory
{
    /**
     * @var array
     */
    private $modifiers = [
        'eq' => EqualTo::class,
        'neq' => NotEqualTo::class,
        'sort' => OrderBy::class,
        'selectOnly' => SelectOnly::class,
        'notNull' => NotNull::class,
        'isNull' => IsNull::class,
        'in' => In::class,
        'notIn' => NotIn::class,
        'lte' => LessThanOrEqualTo::class,
        'lt' => LessThan::class,
        'gte' => GreaterThanOrEqualTo::class,
        'gt' => GreaterThan::class,
    ];


    /**
     * @param string $filterType
     * @return Modifier
     * @throws \Exception
     */
    public function create(string $filterType) : Modifier
    {
        if (!isset($this->modifiers[$filterType])) {
            throw new \Exception("undefined filter");
        }

        return new $this->modifiers[$filterType]();
    }
}
