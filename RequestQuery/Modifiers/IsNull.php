<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Modifiers;

use Illuminate\Database\Eloquent\Builder;

final class IsNull implements Modifier
{
    public function modify(Builder $builder, string $field, $value = null): Builder
    {
        return $builder->whereNull($field);
    }
}
