<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Handlers;

use App\Http\RequestQuery\Modifiers\ModifierFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

final class FieldsetHandler implements Handler
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var ModifierFactory
     */
    private $modifierFactory;

    public function __construct(ModifierFactory $modifierFactory, Request $request)
    {
        $this->modifierFactory = $modifierFactory;
        $this->request = $request;
    }

    /**
     * Applies sparse fieldset to the builder
     *
     * Usage: ?fields=trailer_number,license_plate
     *
     * @param Builder $builder
     * @return Builder
     * @throws \Exception
     */
    public function applyTo(Builder $builder): Builder
    {
        $queryFields = $this->request->get('fields');
        if (null !== ($queryFields)) {
            $builder = $this->modifierFactory->create('selectOnly')->modify($builder, $queryFields);
        }

        return $builder;
    }
}
