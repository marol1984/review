<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Handlers;

use App\Http\RequestQuery\Modifiers\ModifierFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

final class FilteringHandler implements Handler
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var ModifierFactory
     */
    private $modifierFactory;

    public function __construct(ModifierFactory $modifierFactory, Request $request)
    {
        $this->modifierFactory = $modifierFactory;
        $this->request = $request;
    }

    /**
     * Applies filtering to the builder
     *
     * Usage: ?filter[trailer_number][lte] = 10080
     *
     * @param Builder $builder
     * @return Builder
     * @throws \Exception
     */
    public function applyTo(Builder $builder): Builder
    {
        $queryFilters = $this->request->get('filter') ?? [];
        foreach ($queryFilters as $fieldName => $filters) {
            foreach ($filters as $filterType => $value) {
                $builder = $this->modifierFactory->create($filterType)->modify($builder, $fieldName, $value);
            }
        }

        return $builder;
    }
}
