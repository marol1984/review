<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Handlers;

use Illuminate\Database\Eloquent\Builder;

interface Handler
{
    public function applyTo(Builder $builder): Builder;
}
