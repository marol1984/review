<?php

declare(strict_types=1);

namespace App\Http\RequestQuery\Handlers;

use App\Http\RequestQuery\Modifiers\ModifierFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

final class SortingHandler implements Handler
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var ModifierFactory
     */
    private $modifierFactory;

    public function __construct(ModifierFactory $modifierFactory, Request $request)
    {
        $this->modifierFactory = $modifierFactory;
        $this->request = $request;
    }

    /**
     * Applies sorting to the builder.
     *
     * Usage: ?sort=-created_at,+license_plate
     *
     * @param Builder $builder
     *
     * @throws \Exception
     *
     * @return Builder
     */
    public function applyTo(Builder $builder): Builder
    {
        $querySorters = explode(',', $this->request->get('sort') ?? '');
        if (collect($querySorters)->filter()->isEmpty()) {
            return $builder;
        }

        foreach ($querySorters as $sorter) {
            $builder = $this->modifierFactory->create('sort')
                ->modify($builder, ltrim($sorter, '+-'), '-' === $sorter[0] ? 'desc' : 'asc')
            ;
        }

        return $builder;
    }
}
