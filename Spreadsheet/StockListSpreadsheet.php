<?php declare(strict_types=1);

namespace Heisterkamp\UsedTrucks\InventoryManagement\Infrastructure\Spreadsheet;

use Heisterkamp\UsedTrucks\InventoryManagement\Domain\Model\ValueObject\SellingPrice;
use Heisterkamp\UsedTrucks\InventoryManagement\Domain\Model\ValueObject\VehicleId;
use Heisterkamp\UsedTrucks\InventoryManagement\Domain\Model\VehicleRepository;
use Heisterkamp\UsedTrucks\InventoryManagement\Domain\Model\VehicleWorkFlow;
use Heisterkamp\UsedTrucks\InventoryManagement\Domain\Model\VehicleWorkFlowRepository;
use Heisterkamp\UsedTrucks\InventoryManagement\Domain\Services\TotalAmountProvider;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class StockListSpreadsheet extends Spreadsheet
{
    /**
     * @var VehicleRepository
     */
    private $vehicleRepository;

    /**
     * @var VehicleWorkFlowRepository
     */
    private $workFlowRepository;
    private $totalAmountProvider;
    private $router;
    private $hostname;

    /**
     * StockListSpreadsheet constructor.
     *
     * @param VehicleRepository     $vehicleRepository
     * @param UrlGeneratorInterface $router
     * @param string                $hostname
     */
    public function __construct(
        VehicleRepository $vehicleRepository,
        VehicleWorkFlowRepository $workFlowRepository,
        TotalAmountProvider $totalAmountProvider,
        UrlGeneratorInterface $router,
        string $hostname
    ) {
        parent::__construct();
        $this->vehicleRepository = $vehicleRepository;
        $this->workFlowRepository = $workFlowRepository;
        $this->totalAmountProvider = $totalAmountProvider;
        $this->router = $router;
        $this->hostname = $hostname;
    }

    /**
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    private function build()
    {
        $sheet = $this->getActiveSheet();
        $columnIndex = 'A';
        $rowIndex = 1;

        $stockList = $this->vehicleRepository->getStockSummaryList();
        $columnNames = array_keys($stockList[$this->arrayKeyFirst($stockList)]);
        foreach ($columnNames as $columnName) {
            $sheet->getCell($columnIndex."{$rowIndex}")->setValue($columnName);
            $sheet->getCell($columnIndex."{$rowIndex}")->getStyle()->getFont()->setBold(true);
            $sheet->getColumnDimension($columnIndex)->setAutoSize(true);
            ++$columnIndex;
        }

        $this->setColumnsNumberFormat(
            ['M', 'N', 'O', 'P', 'Q', 'R', 'U', 'X', 'Y', 'Z', 'AA', 'AB', 'AC'],
            NumberFormat::FORMAT_NUMBER_00
        );

        foreach ($stockList as $stockItem) {
            ++$rowIndex;
            $columnIndex = 'A';
            $this->router->getContext()->setHost($this->hostname)->setScheme('https');

            $workflow = $this->workFlowRepository
                ->byVehicleId(VehicleId::fromString($stockItem['VehicleId']))
            ;

            $workflow->setTotalAmountProvider($this->totalAmountProvider);

            $stockItem['URL'] = $this->url($stockItem['VehicleId']);
            $stockItem['FullyPaid'] = $this->fullyPaid($workflow);
            $stockItem['OutstandingAmount'] = $this->outstandingAmount($workflow);
            $stockItem['DaysInStock'] = $this->daysInStock($workflow, $stockItem);

            foreach ($columnNames as $columnName) {
                $sheet->getCell("{$columnIndex}{$rowIndex}")->setValue($stockItem[$columnName]);
                ++$columnIndex;
            }
        }

        $sheet->setAutoFilter(
            $sheet->calculateWorksheetDimension()
        );
    }

    private function url(string $vehicleId): string
    {
        return $this->router->generate(
            'heisterkamp_asset_status',
            ['vehicleId' => $vehicleId],
            Router::ABSOLUTE_URL
        );
    }

    private function fullyPaid(VehicleWorkFlow $workflow): string
    {
        if (false === $workflow->depositWasReceived()) {
            return 'N/A';
        }

        return $workflow->remainingAmountToPay() > 0 ? 'NO' : 'YES';
    }

    private function outstandingAmount(VehicleWorkFlow $workflow): string
    {
        if (false === $workflow->preorderWasMade()) {
            return '';
        }

        return SellingPrice::fromInteger($workflow->remainingAmountToPay())->decimal();
    }


    public function daysInStock(VehicleWorkFlow $workFlow, array $stockItem): string
    {
        $purchasePaymentExecutedAt = date_create($stockItem['PurchasePaymentExecutedAt'] ?? '');
        $lastPayment = date_create($stockItem['LastPayment'] ?? '');

        if ($workFlow->orderWasMade() && $workFlow->isOrderTotalPaidByCustomer()) {
            return date_diff($lastPayment, $purchasePaymentExecutedAt)->format('%a');
        }

        return date_diff(new \DateTime(), $purchasePaymentExecutedAt)->format('%a');
    }

    /**
     * @param array $arr
     *
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     *
     * @return null|int|string
     */
    private function arrayKeyFirst(array $arr)
    {
        foreach ($arr as $key => $unused) {
            return $key;
        }

        return null;
    }

    private function setColumnsNumberFormat(array $columns, string $format): void
    {
        foreach ($columns as $column) {
            $this->getActiveSheet()->getStyle($column.':'.$column)
                ->getNumberFormat()
                ->setFormatCode($format)
            ;
        }
    }
}
