<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use App\Http\RequestQuery\RequestQuery;
use App\Models\Truck\Truck;
use App\Repositories\Database\Common\Criteria\ApplyRequestQuery;
use App\Repositories\Database\Cweaiv75\TruckCustomerTimeTableRepository;
use App\Repositories\Database\Truck\Criteria\PositionTimeStartingFrom;
use App\Repositories\Database\Truck\TruckPositionRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Carbon;

final class TruckPositionController extends Controller
{
    /** @var TruckPositionRepository $truckPositionRepository */
    private $truckPositionRepository;

    /** @var TruckCustomerTimeTableRepository $truckCustomerTimeTableRepository */
    private $truckCustomerTimeTableRepository;

    /**
     * TruckController constructor.
     *
     * @param TruckPositionRepository          $truckPositionRepository
     * @param TruckCustomerTimeTableRepository $truckCustomerTimeTableRepository
     */
    public function __construct(
        TruckPositionRepository $truckPositionRepository,
        TruckCustomerTimeTableRepository $truckCustomerTimeTableRepository
    ) {
        $this->truckPositionRepository = $truckPositionRepository;
        $this->truckCustomerTimeTableRepository = $truckCustomerTimeTableRepository;
    }

    /**
     * Fetch positions of a truck.
     *
     * @param Truck $truck
     *
     * @return JsonResponse
     */
    public function index(Truck $truck, RequestQuery $requestQuery): JsonResponse
    {
        $this->truckPositionRepository
            ->pushCriteria(new PositionTimeStartingFrom(Carbon::today()))
            ->pushCriteria(new ApplyRequestQuery($requestQuery));

        if (auth()->user()->intern === false) {
            $this->truckPositionRepository->pushCriteria(
                new PositionTimeStartingFrom(
                    $this->truckCustomerTimeTableRepository->assignedStartDateTimeFor($truck)
                )
            );
        }

        $positions = $this->truckPositionRepository->byTruck($truck);

        return response()->json($positions, 200);
    }
}
