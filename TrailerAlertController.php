<?php

declare(strict_types=1);

namespace App\Http\Controllers\V1;

use Illuminate\Http\JsonResponse;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Trailer\Trailer;

final class TrailerAlertController extends Controller
{
    /**
     * Return the alerts of a specific trailer
     *
     * @param Trailer $trailer
     * @return JsonResponse
     */
    public function index(Trailer $trailer, RequestQuery $requestQuery): JsonResponse
    {
        $trailerAlerts = $trailer
            ->alerts()
            ->where('alert_time', '>=', Carbon::today())
            ->with('trailer.customerTrailerInfo')
            ->get();

        $trailerAlerts = $requestQuery->applyTo($trailerAlerts);

        return response()->json($trailerAlerts);
    }
}
