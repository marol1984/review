<?php

declare(strict_types=1);

namespace App\Repositories\Database\Cweaiv75;

use App\Repositories\Database\Truck\Criteria\WithoutPlanGroups;
use App\Repositories\Database\Truck\Criteria\WithTruckNumbers;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\HasOne;

final class TruckRepository extends Repository
{
    /** @var Builder */
    protected $model;

    /**
     * Specify the model for this repository.
     *
     * @return string
     */
    public function model(): string
    {
        return 'App\Models\Truck\Truck';
    }

    /**
     * Fetch trucks by truck numbers
     *
     * @param \Illuminate\Support\Collection $truckNumbers
     * @return mixed
     */
    public function byTruckNumbers(\Illuminate\Support\Collection $truckNumbers)
    {
        $this->pushCriteria(new WithoutPlanGroups(['66','88','99','FLE']));
        $this->pushCriteria(new WithTruckNumbers($truckNumbers));

        return $this->all();
    }

    /**
     * Fetch all trucks.
     * Excluding specific plan groups, because these trucks aren't in use anymore
     *
     * @param \Illuminate\Support\Collection
     * @return Collection
     */
    public function withDriverAndActivity(\Illuminate\Support\Collection $truckNumbers): Collection
    {
        $this->pushCriteria(new WithoutPlanGroups(['66','88','99','FLE']));

        $this->applyCriteria();
        return $this->model
            ->with(['driver' => function(HasOne $query) {
                return $query->select(['drivernumber', 'bv_nationality', 'firstname', 'lastname_no_initials']);
            }])
            ->with(['activity' => function(HasOne $query) {
                return $query->select(['act_code', 'act_desc', 'act_desc_', 'act_desc_nl', 'act_desc_de', 'act_desc_pl', 'act_desc_gb']);
            }])
            ->whereIn('truck_number', $truckNumbers)
            ->get();
    }

    /**
     * Fetch all trucks.
     * Excluding specific plan groups, because these trucks aren't in use anymore
     *
     * @return Collection
     */
    public function withDriverAndActivityAndCustomer(): Collection
    {
        $this->pushCriteria(new WithoutPlanGroups(['66','88','99','FLE']));

        $this->applyCriteria();
        return $this->model
            ->with(['driver' => function(HasOne $query) {
                return $query->select(['drivernumber', 'bv_nationality', 'firstname', 'lastname_no_initials']);
            }])
            ->with(['activity' => function(HasOne $query) {
                return $query->select(['act_code', 'act_desc', 'act_desc_', 'act_desc_nl', 'act_desc_de', 'act_desc_pl', 'act_desc_gb']);
            }])
            ->with('customer')
            ->get();
    }
}
